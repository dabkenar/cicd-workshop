# Smoke testing

Smoke testing is a fail-fast validation that checks if the application is deployed correctly.
This is done directly after a deployment, and usually features only the bare minimum of verification to ensure our deployment
has happened successfully. 

## Goals

By adding smoke tests to a continuous delivery pipeline we're checking whether a deployment to an environment 
happened correctly. If not these tests should fail, notifying us that something went wrong. 

## Approach

A smoke test is very simple for a web application and can be easily done with curl.

## Exercise

In this exercise a smoke test job and stage are created which validate the endpoint of the application with curl.

### Step 1: Smoke test

Create a smoke_test job and a smoke stage in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - deploy
  - test
  - smoke

# The smoke test job
smoke_test:
  stage: smoke
  script:
  - curl -X POST $TRIANGLE_URL -d @smoke-test.json
```

The stage **smoke** is an new logical divider of steps within the continuous delivery pipeline. The job **smoke_test** is 
actual command that starts the smoke test. 

### Step 2: Verify results and move on
Like before, commit your change with a relevant message and check out your handiwork.
You've just verified whether our deployment actually lead to a working application! (or at least, we're trying to ;) ) 

If everything happened successfully, move on to 
[the next exercise: User Acceptance Testing!](../exercises/user-acceptance-testing.md). 
A very difficult but rewarding step to automate! 