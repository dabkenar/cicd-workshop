'use strict';

const implementation = require('./implementation');

/*
 Create a Facade between step definitions and actual interactions to make changing technologies easy without changing
 the test definitions. This Facade could be reused for performance testing for example.
  */


var Facade = function () {

};


Facade.prototype.prepareEnvironment = () => {
    implementation.prepareForTriangleCalculation();
};

Facade.prototype.doTriangleCalculation = async (numberOfEqualSides) => {
    console.log("Calculate triangle type for number of equal side", numberOfEqualSides)
    return await implementation.doCalculationRequest(numberOfEqualSides);
};


Facade.prototype.validateExpectedTriangleType = (expectedTriangleType) => {
    console.log("Expected triangle type", expectedTriangleType);
};

module.exports = new Facade();